#require

#$
require 'node-jquery-lite'

#db
require './server/db'

#router
require './server/router/router'

#log
$.info 'success', 'server is ready, in ' + ($.now() - $.st) + ' ms'