#============
#require
#============
exec = (require 'child_process').exec

gulp = require 'gulp'
gutil = require 'gulp-util'
watch = require 'gulp-watch'
plumber = require 'gulp-plumber'
replace = require 'gulp-replace'
clean = require 'gulp-clean'
ignore = require 'gulp-ignore'
concat = require 'gulp-concat'
rename = require 'gulp-rename'

uglify = require 'gulp-uglify'

jade = require 'gulp-jade'
coffee = require 'gulp-coffee'
stylus = require 'gulp-stylus'
cson = require 'gulp-cson'

lint = require 'gulp-coffeelint'

#============
#error
#============
#uncaughtException
process.on 'uncaughtException', (err) -> log err.statck

#============
#function
#============
#log
log = console.log

#============
#task
#============

#coffee
gulp.task 'coffee', ->
  gulp.src ['./**/*.coffee', '!./node_modules/**']
  .pipe plumber()
  .pipe coffee bare: true
  .pipe uglify()
  .pipe gulp.dest './'

#jade
gulp.task 'jade', ['stylus', 'coffee'], ->
  gulp.src ['./**/*.jade', '!./node_modules/**']
  .pipe plumber()
  .pipe jade()
  .pipe gulp.dest './'

#stylus
gulp.task 'stylus', ->
  gulp.src ['./**/*.styl', '!./node_modules/**']
  .pipe plumber()
  .pipe stylus()
  .pipe gulp.dest './'

#cson
gulp.task 'cson', ->
  gulp.src ['./**/*.cson', '!./node_modules/**']
  .pipe plumber()
  .pipe cson()
  .pipe gulp.dest './'

#uglify
gulp.task 'uglify', ->
  gulp.src ['./**/*.js', '!./node_modules/**']
  .pipe plumber()
  .pipe uglify()
  .pipe gulp.dest './'

#watch
gulp.task 'watch', ->

  #sanae
  watch 'server/**/*.jade', ->
    gulp.src './server/**/*.html'
    .pipe clean()

  #nagisa
  watch 'client/nagisa/require/**/*.styl', ->
    gulp.src './client/nagisa/core.styl'
    .pipe stylus()
    .pipe gulp.dest './client/nagisa/'

    gulp.src './client/nagisa/require/**/*.css'
    .pipe clean()

  #lint
  watch ['./**/*.coffee', '!./node_modules/**']
  .pipe lint()
  .pipe lint.reporter()

#build
gulp.task 'build', [
  'cson'
  'jade'
]

#lint
gulp.task 'lint', ->
  gulp.src ['./**/*.coffee', '!./node_modules/**']
  .pipe lint()
  .pipe lint.reporter()