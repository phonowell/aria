#user
$.user = {}

#validate
validate = (p, def, callback) ->
  $.next ->
    #check
    if !(p.email and ($.type p.email) == 'string')
      def.reject '邮箱地址不正确或为空。'
      return

    #if change passoword
    if p.cp
      if !(p.old and p.new)
        def.reject '密码为空。'
        return
      if !(($.type p.old) == 'string' and ($.type p.new) == 'string')
        def.reject '密码格式不正确。'
        return
      if !(6 <= p.old.length <= 20 and 6 <= p.new.length <= 20)
        def.reject '密码长度不正确。'
        return
      if !p.old == p.new
        def.reject '新旧密码相同。'
        return

    #if not reset password
    if !p.rp
      if !(p.password and ($.type p.password) == 'string')
        def.reject '密码格式不正确或为空。'
        return
      if !(6 <= p.password.length <= 20)
        def.reject '密码长度不正确。'
        return



    #callback
    callback()

#decodeError
decodeError = (err) ->
  switch err.code
    when 'EMAIL_TAKEN' then '邮箱地址已被注册。'
    when 'INVALID_EMAIL' then '无法验证的非法邮箱。'
    when 'INVALID_USER' then '用户不存在。'
    when 'INVALID_PASSWORD' then '密码不正确。'
    else err

#create
$.user.create = (p) ->
  #def
  def = new $.Deferred()

  #validate
  validate p, def, ->

    #to db
    $.db.createUser
      email: p.email
      password: p.password
    , (err) ->
      if err
        def.reject decodeError err
        return

      #resolve
      def.resolve()

  #return
  def

#remove
$.user.remove = (p) ->
  #def
  def = new $.Deferred()

  #validate
  validate p, def, ->

    #to db
    $.db.removeUser
      email: p.email
      password: p.password
    , (err) ->
      if err
        def.reject decodeError err
        return

      #resolve
      def.resolve()

  #return
  def

#login
$.user.login = (p) ->
  #def
  def = new $.Deferred()

  #validate
  validate p, def, ->

    #to db
    $.db.authWithPassword
      email: p.email
      password: p.password
    , (err, auth) ->
      if err
        def.reject decodeError err
        return

      #resolve
      def.resolve auth

  #return
  def

#change password
$.user.changePassword = (p) ->
  #def
  def = new $.Deferred()

  #param
  p.cp = true

  #validate
  validate p, def, ->

    #to db
    $.db.changePassword
      email: p.email
      oldPassword: p.old
      newPassword: p.new
    , (err) ->
      if err
        def.reject decodeError err
        return

      #resolve
      def.resolve()

  #return
  def

#reset password
$.user.resetPassword = (p) ->
  #def
  def = new $.Deferred()

  #param
  p.rp = true

  #validate
  validate p, def, ->

    #to db
    $.db.resetPassword
      email: p.email
    , (err) ->
      if err
        def.reject decodeError err
        return

      #resolve
      def.resolve()

  #return
  def